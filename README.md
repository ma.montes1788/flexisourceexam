/storeplayers - Fetch and store a minimum of 100 players from this data provider
/api/getplayers - Get the list of all players with only ID and Full Name.
/getplayerdata/{code} - Get the full data object of a single Player. The object should contain at least but not limit to id, first
name, second name, form, total points, influence, creativity, threat, and ICT index.